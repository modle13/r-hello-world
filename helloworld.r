#!/usr/bin/Rscript

print("hello world")
proc.time()

print("hello world", quote=FALSE, file=stderr())

df = data.frame(a=c(1,2,3),b=c(4,5,6),greet=c("yo","wut","up"))

print(df)


# Create a vector filled with random normal values
u1 <- rnorm(30)
print("This loop calculates the square of the first 10 elements of vector u1")

# Initialize `usq`
usq <- 0

for(i in 1:10) {
  # i-th element of `u1` squared into `i`-th position of `usq`
  usq[i] <- u1[i]*u1[i]
  print(usq[i])
}

print(i)
